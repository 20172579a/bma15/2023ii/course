package pe.edu.uni.cvilchezz.mavenproject1;

import java.util.ArrayList;

public class ArregloService {

    private int[] arreglo;

    public ArregloService(int n) {
        this.arreglo = new int[n];
        for (int i = 0; i < arreglo.length; i++) {
            this.arreglo[i] = (int) (Math.random() * 10 + 21);

        }
    }

    public int[] getArreglo() {
        return arreglo;

    }

    public static ArrayList<Integer> arregloDiferencia(ArregloService A, ArregloService B) {
        boolean rep;
        ArrayList<Integer> m = new ArrayList<Integer>();
        ArrayList<Integer> x = new ArrayList<Integer>();
        int[] a = A.getArreglo();
        int[] b = B.getArreglo();
        for (int i = 0; i < a.length; i++) {
            rep = false;
            for (int j = 0; j < b.length; j++) {

                if (a[i] == b[j]) {
                    rep = true;
                    break;
                }

            }
            if (rep == false) {
                m.add(a[i]);
            }

        }
        boolean band = false;
        for (int i = 21; i < 31; i++) {
            band = false;
            for (int j : m) {
                if (j == i) {
                    band = true;
                    break;
                }
            }
            if (band == true) {
                x.add(i);
            }
        }
        return x;
    }
    public static ArrayList<Integer> arregloInterseccion(ArregloService A, ArregloService B) {
    ArrayList<Integer> m = new ArrayList<Integer>();
    ArrayList<Integer> x = new ArrayList<Integer>();
    boolean rep;
    int []a=A.getArreglo();
    int []b=B.getArreglo();
        for (int i = 21; i < 31; i++) {
            for (int j = 0; j < a.length; j++) {
                if(i==a[j]){
                    for (int k = 0; k < b.length; k++) {
                        if(a[j]==b[k]){
                        m.add(i);
                       
                        }
                    }
                }
                
            }
        }
        boolean band = false;
        for (int i = 21; i < 31; i++) {
            band = false;
            for (int j : m) {
                if (j == i) {
                    band = true;
                    break;
                }
            }
            if (band == true) {
                x.add(i);
            }
        }
    return x;
    }
}
