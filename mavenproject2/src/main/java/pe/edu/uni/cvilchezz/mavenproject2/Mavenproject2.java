/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package pe.edu.uni.cvilchezz.mavenproject2;

import java.util.ArrayList;
import java.util.Scanner;

public class Mavenproject2 {

    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int nota, sum = 0, num = 0, prom, may = 0, men = 0;
        ArrayList<Integer> arreglo = new ArrayList<Integer>();
        do {
            System.out.println("Ingrese una nota o un numero negativo para terminar:");
            nota = entrada.nextInt();
            if(nota>=0){
            arreglo.add(nota);
            sum += nota;
            num++;
            }
        } while (nota > 0);

        prom = sum / num;
        System.out.println("El promedio es:" + prom);
        for (int i = 0; i < arreglo.size(); i++) {
            if (arreglo.get(i) > prom) {
                may++;
            } else {
                men++;
            }
        }
        System.out.println("cantidad de mayores al promedio:"+may);
        System.out.println("cantidad menores al promedio:"+men);
    }
}
