package pe.edu.uni.cvilchezz.classes;

/**
 *
 * @author Christopher Vilchez <cvilchezz@uni.pe>
 */
public class Circle {

    double radious;

    Circle() {
        this.radious = 1;
    }

    Circle(double newRadious) {
        this.radious = newRadious;
    }

    double getArea() {
        return Math.PI * this.radious * this.radious;
    }

    double getPerimeter() {
        return 2 * Math.PI * this.radious;
    }

    void setRadious(double newRadious) {
        this.radious = newRadious;
    }

    @Override
    public String toString() {
        return "Circle{" + "radious=" + radious + '}';
    }

   
}
