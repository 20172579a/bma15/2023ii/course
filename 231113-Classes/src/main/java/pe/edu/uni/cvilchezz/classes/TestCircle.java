

package pe.edu.uni.cvilchezz.classes;

/**
 *
 * @author Christopher Vilchez <cvilchezz@uni.pe>
 */
public class TestCircle {

    public static void main(String[] args) {
        System.out.println("Test Circle!");
        Circle circle1;
        circle1= new Circle();
        System.out.println(circle1.toString());
        System.out.println("perimeter 1: "+ circle1.getPerimeter());
        System.out.println("Area 1: "+circle1.getArea());
        double radious2=25;
        Circle circle2;
        circle2=new Circle(radious2);
        System.out.println(circle2.toString());
        System.out.println("perimeter 2: "+ circle2.getPerimeter());
        System.out.println("Area 2: "+circle2.getArea());
        
        Circle circle3=new Circle();
        double radious3=125;
        circle3.setRadious(radious3);
        System.out.println(circle3.toString());
        System.out.println("perimeter 3: "+ circle3.getPerimeter());
        System.out.println("Area 3: "+circle3.getArea());
        
    }
}
