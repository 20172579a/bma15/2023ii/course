/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package pe.edu.uni.cvilchezz.mavenproject3;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Christopher Vilchez <cvilchezz@uni.pe>
 */
public class Mavenproject3 {

    public static void main(String[] args) {
        Scanner entrada=new Scanner(System.in);
        ArrayList<String> apellido=new ArrayList<String>();
        
        System.out.println("Ingrese la cantidad de apellidos:");
        int num=entrada.nextInt();
        int n=0;
        while(n<num){
            System.out.println("Ingrese appelido:");
            String cadena=entrada.next();
            apellido.add(cadena);
            n++;
        } 
        String temp;
        String[] cad=new String[apellido.size()];
        int j=0;
        for (String i :apellido ) {
            cad[j]=i;
            j++;
        }
        for (int i = 0; i < cad.length; i++) {
            for ( j = i+1; j < cad.length; j++) {
                if(cad[j].compareToIgnoreCase(cad[i])<0){
                temp=cad[i];
               cad[i]=cad[j];
               cad[j]=temp;
                }
            }
        }
        System.out.println("%nAregglo ordenado alfabeticamente:");
        for (String i: cad) {
            System.out.print(i+", ");
        }
    }
}
